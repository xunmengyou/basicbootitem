package com.wwy.Dao;

import com.wwy.Entity.Person;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonMapper {
    public void insertPerson(Person person);
    public Person selectPersonById(Integer id);
    public Integer delPerson(Person person);
}
