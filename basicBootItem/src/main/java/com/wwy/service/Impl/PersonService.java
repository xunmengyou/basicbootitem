package com.wwy.service.Impl;

import com.wwy.Dao.PersonMapper;
import com.wwy.Entity.Person;
import com.wwy.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 18117430 on 2019/3/13.
 */

@Service
public class PersonService implements IPersonService {

    @Autowired
    private PersonMapper personMapper;

    @Override
    public Person getPersonById(Integer id) {
        Person person = personMapper.selectPersonById(id);
        return person;
    }

    /**
     * 某个用户 插入的记录
     * @param userId
     * @param person
     */
    @Override
    public void insertPerson(String userId,Person person){
        personMapper.insertPerson(person);
    }

    @Override
    public void delPerson(String userId, Person person) {
         personMapper.delPerson(person);
    }


}
