package com.wwy.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication = (默认属性)@Configuration + @EnableAutoConfiguration + @ComponentScan。
//@Configuration：提到@Configuration就要提到他的搭档@Bean
//@EnableAutoConfiguration：能够自动配置spring的上下文，试图猜测和配置你想要的bean类，通常会自动根据你的类路径和你的bean定义自动配置
//@ComponentScan：会自动扫描指定包下的全部标有@Component的类，并注册成bean，当然包括@Component下的子注解@Service,@Repository,@Controller

@SpringBootApplication(scanBasePackages= {"com.wwy"})
//@SpringBootApplication(scanBasePackages="com.test.quartz")
//mapper 接口类扫描包配置
@MapperScan(basePackages = {"com.wwy.Dao","com.wwy.controller","com.wwy.service"}) //可以测试各个层
public class ItemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemApplication.class, args);
	}

}
