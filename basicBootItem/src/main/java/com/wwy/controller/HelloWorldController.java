/**
 * 
 */
package com.wwy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangwy
 *
 */

@RestController
public class HelloWorldController {
	@RequestMapping("/")  
	   public String sayHello() {  
	       return "Hello,World!";  
	} 

}
