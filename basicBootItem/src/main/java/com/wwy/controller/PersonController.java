package com.wwy.controller;

import com.alibaba.fastjson.JSON;
import com.wwy.Entity.Person;
import com.wwy.service.Impl.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/Person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping("/getPersonById")
    @ResponseBody
    public Object getPersonById(){
        Person person = personService.getPersonById(1);
        return JSON.toJSONString(person);
    }
}
